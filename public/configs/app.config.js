/* eslint-disable */
const configCreate = () => {
  function upToDecimal(number, count = 2) {
    const maxCount = 10;
    const coeficient = count <= maxCount
      ? 10 ** count
      : maxCount;
    return Math.ceil(number * coeficient) / coeficient;
  }

  // Конфигурация сообщений об ошибках
  const messages = {
    errors: {
      multByZero: 'Произведение на нуль всегда дает нуль',
      divByZero: 'Делить на нуль нельзя',
    },
  };

  // Возвращаем главный объект кастомной конфигурации
  // Вы можете настраивать приложение изменяя значения параметров
  return {
    operators: {
      add: {
        isShown: true,
        text: 'Сложить',
        symbol: '+',
        action: (arg1, arg2) => {
          return upToDecimal(arg1 + arg2, 5);
        },
      },
      sub: {
        isShown: true,
        text: 'Вычесть',
        symbol: '-',
        action: (arg1, arg2) => {
          return upToDecimal(arg1 - arg2, 5);
        },
      },
      mult: {
        isShown: true,
        text: 'Умножить',
        symbol: '×',
        action: (arg1, arg2) => {
          if (arg1 === 0 || arg2 === 0) {
            alert(messages.errors.multByZero);
          }
          return upToDecimal(arg1 * arg2, 5);
        },
      },
      div: {
        isShown: true,
        text: 'Разделить',
        symbol: '/',
        action: (arg1, arg2) => {
          if (arg2 === 0) {
            alert(messages.errors.divByZero);
            return;
          }
          return upToDecimal(arg1 / arg2, 5);
        },
      },
    },
  };
};
