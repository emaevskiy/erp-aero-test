import Vue from 'vue';
import App from './App.vue';
import configSetup from './mixins/configSetup';

Vue.config.productionTip = false;

/* eslint-disable no-undef */
const config = typeof configCreate === 'function'
  ? configSetup(configCreate())
  : null;

new Vue({
  config,
  render: h => h(App),
}).$mount('#app');
