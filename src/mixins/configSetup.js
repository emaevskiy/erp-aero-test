import Vue from 'vue';

Vue.mixin({
  beforeCreate() {
    const options = this.$options;
    if (options.config) {
      this.$config = options.config;
    } else if (options.parent && options.parent.$config) {
      this.$config = options.parent.$config;
    }
  },
});

const configSetup = (obj) => {
  if (!typeof obj === 'object' || obj === null || Array.isArray(obj)) {
    /* eslint-disable no-console */
    console.warn('[Vue-CreateConfig-Mixin:] config can not be create', obj, 'is not an [Object]');
    return null;
  }
  return obj;
};

export default configSetup;
